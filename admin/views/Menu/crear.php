<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Quick Example</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="menuForm" role="form">
              <div class="box-body">
                <div class="form-group">
                  <label for="menuName">Nombre</label>
                  <input type="text" name="name" class="form-control" id="menuName" placeholder="Nombre menú">
                </div>
                <div class="form-group">
                  <label for="menuUrl">Url:</label>
                  <input type="text" name="url" class="form-control" id="menuUrl" placeholder="URL">
                </div>

                <div class="form-group">
                  <label>Padre:</label>
                  <select name="parent" class="form-control">
                      <option value="0">Seleccione una opción</option>
                    <?php foreach ($this->menus as $key => $menu) : ?>
                        <option value="<?php echo $menu['id']; ?>"><?php echo $menu["name"]; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>

          <?php $this->asyncCreation("#menuForm","Menu/create","Menu"); ?>