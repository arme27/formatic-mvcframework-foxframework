<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index_controller
 *
 * @author pabhoz
 */
class Menu_controller extends \Fox\FoxController{

    function __construct() {
        parent::__construct();
    }

    public function index()
    {
      $this->view->menus = Menus_bl::getMenus();
      $this->view->render($this,"index");
    }
    
    public function crear()
    {
        $this->view->menus = Menus_bl::getMenus();
        $this->view->render($this,"crear");
    }
    
    public function create(){
        $data = filter_input_array(INPUT_POST);
        $data["id"] = null;
        $data["parent"] = ($data["parent"] > 0) ? $data["parent"] : null;
        $menu = Menu::instanciate($data);
        $r = $menu->create();
        print_r(json_encode($r));
    }

     public function update(){
         $data = filter_input_array(INPUT_POST);
         $menu = Menu::getById($data["id"]);
         unset($data["id"]);
         foreach ($data as $key => $value) {
             $menu->{"set".ucfirst($key)}($value);
         }
         $r = $menu->update();
         print_r(json_encode($r));
     }
     
     public function delete(){
        // print_r($_POST);
        $id = filter_input(INPUT_POST, "id");
        $r= Menu::getById($id)->delete();
        print_r(json_encode($r));
     }
}
