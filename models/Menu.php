<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Model
 *
 * @author Pabhoz
 */
class Menu extends \Fox\FoxModel {

  private $id;
  private $name;
  private $url;
  private $parent;

  function __construct($id,$name,$url,$parent) {
        parent::__construct();
        $this->id = $id;
        $this->name = $name;
        $this->url = $url;
        $this->parent = $parent;
  }

    public function getId(){
        return $this->id;
    }

    public function getName(){
        return $this->name;
    }
    public function getUrl(){
        return $this->url;
    }
    public function getParent(){
        return $this->parent;
    }

    public function setId($value){
        $this->id = $value;
    }
    public function setName($value){
        $this->name = $value;
    }
    public function setUrl($value){
        $this->url = $value;
    }
    public function setParent($value){
        $this->parent = $value;
    }

    public function getMyVars(){
        return get_object_vars($this);
    }

}
